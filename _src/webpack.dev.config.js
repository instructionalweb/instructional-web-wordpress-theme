const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require("postcss-preset-env");

module.exports = {
  entry: {
    "iw-scripts": "./js/iw-scripts.js",
    "iw-admin-scripts": "./js/iw-admin-scripts.js",
    style: "./js/style.js", // Necessary entrypoint to get webpack to generate css file
  },

  mode: "development",
  devtool: "inline-source-map",
  watch: true,

  watchOptions: {
    ignored: /node_modules/,
  },
  externals: {
    jquery: "jQuery",
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  postcssPresetEnv({
                    browsers: ["last 2 versions", "ie >= 9", "and_chr >= 2.3"],
                  }),
                ],
              },
            },
          },
          "sass-loader",
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-react", "@babel/preset-env"],
          },
        },
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "../style.css",
    }),
  ],
  stats: {
    loggingDebug: ["sass-loader"],
  },
  output: {
    filename: "[name].js",
    // filename: "[name].bundle.js",
    path: path.resolve(__dirname, "../js"),
    // clean: true,
  },
};
