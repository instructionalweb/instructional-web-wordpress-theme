jQuery(function() {
    const additionalCSSSubmenu = document.querySelector('a[href="themes.php?page=editcss-customizer-redirect"]');
    // Hide Additional CSS from Admin menu
    additionalCSSSubmenu.parentElement.style.display = 'none';
})