import $ from "jquery";
import objectFitImages from "object-fit-images";
import "imagesloaded";
import "flickity";

// Testing

const searchForm = document.querySelector(".searchForm");
const searchSubmitBtn = document.querySelector(
  '.searchForm button[type="submit"]'
);
const menuToggle = document.getElementById("menuToggle");
const siteNavigation = document.getElementById("siteNavigation");
let siteNavigationList = document.getElementById("mainMenu");
const mobileNavCloseBtn = document.getElementById("mobileNavCloseBtn");
const covidButton = document.getElementById("covid-button");

window.addEventListener("dropdownMenuLoaded", () => {
  console.log("dropdown menu all loaded up and ready to go.");
  siteNavigationList = document.querySelector("#siteNavigation ul");

  if (window.matchMedia("(min-width: 40em)").matches) {
    siteNavigation.setAttribute("aria-expanded", true);
    siteNavigationList.removeAttribute("hidden");
  } else {
    siteNavigation.setAttribute("aria-expanded", false);
    siteNavigationList.setAttribute("hidden", "");
  }
});

window.addEventListener("closeBtnLoaded", () => {
  const closeMenuBtn = document.querySelector("iw-close button");

  closeMenuBtn.addEventListener("click", () => {
    toggleMenu();
  });
});

window.addEventListener("resize", () => {
  if (window.matchMedia("(min-width: 40em)").matches) {
    siteNavigation.setAttribute("aria-expanded", true);
    siteNavigationList.removeAttribute("hidden");
    document.body.classList.remove("menuOpen");
  } else {
    siteNavigation.setAttribute("aria-expanded", false);
    siteNavigationList.setAttribute("hidden", "");
  }
});

searchForm.addEventListener("focusin", () => {
  searchForm.classList.add("focus");
  console.log("form has focus");
});

searchForm.addEventListener("focusout", () => {
  searchForm.classList.remove("focus");
});

searchSubmitBtn.addEventListener("click", (e) => {
  console.log("value", searchForm.elements.searchField.value);
  if (searchForm.elements.searchField.value === "") {
    e.preventDefault();
    return false;
  }
});

menuToggle.addEventListener("click", () => {
  toggleMenu();
});

function toggleMenu() {
  console.log("menu toggle");

  if (siteNavigation.getAttribute("aria-expanded") === "true") {
    siteNavigation.setAttribute("aria-expanded", false);
    siteNavigationList.setAttribute("hidden", "");
    document.body.classList.remove("menuOpen");
    return;
  }

  siteNavigation.setAttribute("aria-expanded", true);
  siteNavigationList.removeAttribute("hidden");
  document.body.classList.add("menuOpen");
}

mobileNavCloseBtn.addEventListener("click", () => {
  toggleMenu();
});

// Covid Cookie
if (covidButton) {
  covidButton.addEventListener("click", () => {
    setCookie();
  });
}

var covidBox = document.getElementById("covid-notification");

function hideCovidBox() {
  if (covidBox) {
    covidBox.style.display = "none";
  }
}

function setCookie() {
  hideCovidBox();
  // Local Storage Code
  // localStorage.setItem('covidDefeated', "yes");

  let exp = new Date(Date.now() + 86400e3);
  exp = exp.toUTCString();
  document.cookie = `covidDefeated=yes;expires=${exp};path=/`;
}

function checkForCovidCookie() {
  return document.cookie
    .split(";")
    .some((item) => item.trim().startsWith("covidDefeated="));
}
function checkCookieOnLoad() {
  if (checkForCovidCookie()) {
    //Local Storage code was localStorage.getItem("covidDefeated")=="yes"
    hideCovidBox();
  } else {
    if (covidBox) {
      covidBox.style.display = "flex";
    }
  }
}

function getCovidText() {
  $.getJSON(
    "https://instruction.austincc.edu/wp-json/wp/v2/pages/364",
    function (data) {
      console.log("COVIDALERT: ", data.content.rendered);
      $("#covid-text").html(data.content.rendered);
    }
  );
}

$(document).ready(function () {
  $(document).foundation();
  objectFitImages();
  siteNavigation.style.display = "block";
  checkCookieOnLoad();
  getCovidText();
  // $('.main-carousel').flickity({
  //   // options
  //   cellAlign: 'left',
  //   contain: true
  // });
  // const menu = new Foundation.DropdownMenu($('#mainMenu'), {
  //   disableHover: true,
  //   clickOpen: true
  // });
  // console.log('menu should be created now', menu);
});
