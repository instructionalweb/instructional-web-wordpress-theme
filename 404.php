<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$context['footer_widgets'] = Timber::get_widgets( 'footer_widgets' );
$context['header_widgets'] = Timber::get_widgets( 'header_widgets' );
Timber::render( '404.twig', $context );
