<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
    add_theme_support( 'menus' );

     // Set content width value based on the theme's design
    if ( ! isset( $content_width ) ) {
      $content_width = 600;
    }
    
    /*
    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
    add_theme_support( 'html5', array(
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    /*
    * Enable support for Post Formats.
    *
    * See: https://codex.wordpress.org/Post_Formats
    */
    add_theme_support( 'post-formats', array(
      'aside',
      'image',
      'video',
      'quote',
      'link',
      'gallery',
      'audio',
    ) );
  
    // Add theme support for Custom Logo.
    add_theme_support( 'custom-logo', array(
      'width'       => 250,
      'height'      => 250,
      'flex-width'  => true,
    ) );

    $defaults = array(
      'default-image' => '',
      'random-default' => false,
      'width' => 0,
      'height' => 0,
      'flex-height' => false,
      'flex-width' => false,
      'default-text-color' => '#fff',
      'header-text' => true,
      'uploads' => true,
      'wp-head-callback' => '',
      'admin-head-callback' => '',
      'admin-preview-callback' => '',
      'video' => false,
      'video-active-callback' => 'is_front_page',
    );
    // add_theme_support( 'custom-header', $defaults );


		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
    add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
    add_filter('wp_kses_allowed_html', array( $this, 'add_allowed_tags'), 10, 4);
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );

    add_action( 'wp_enqueue_scripts', array( $this, 'loadScripts' ) );
    add_action('admin_enqueue_scripts', array( $this, 'admin_style') );
    add_action( 'customize_register', 'iw_customize_register' );

    function iw_role_admin_body_class( $classes ) {
      global $current_user;
      foreach( $current_user->roles as $role )
          $classes .= ' role-' . $role;
      return trim( $classes );
    }
    add_filter( 'admin_body_class', 'iw_role_admin_body_class' );


    function get_main_menu() {
      return wp_get_nav_menu_items('Main Menu');
    }

    function get_footer_menu() {
      return wp_get_nav_menu_items('footer links');
    }

    add_action( 'rest_api_init', function() {
      register_rest_route( 'myroutes', '/mainmenu', array(
        'methods' => 'GET',
        'callback' => 'get_main_menu',
      ) );

      register_rest_route( 'myroutes', '/footermenu', array(
        'methods' => 'GET',
        'callback' => 'get_footer_menu',
      ) );
    });

    add_action( 'widgets_init', function () {
      register_sidebar( array(
        'name'          => 'Footer Widgets',
        'id'            => 'footer_widgets',
        'before_widget' => '<div class="footerWidget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
      ) );

      register_sidebar( array(
        'name'          => 'Header Widgets',
        'id'            => 'header_widgets',
        'before_widget' => '<div class="headerWidget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
      ) );

      register_sidebar( array(
        'name'          => 'Sidebar Widgets',
        'id'            => 'sidebar_widgets',
        'before_widget' => '<div class="sidebarWidget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
      ) );
    });

		parent::__construct();
  }
  
  function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'/css/admin.css');
    wp_enqueue_script('iw-admin-scripts', get_template_directory_uri().'/js/iw-admin-scripts.js');
  }
 
	function loadScripts() {
    wp_enqueue_script( 'polyfills', 'https://cdn.polyfill.io/v3/polyfill.min.js', array(), false, false);
    // wp_enqueue_script( 'app-core', 'https://unpkg.com/iw-components@0.0.2/dist/instructional-web.js' );
    // wp_enqueue_script( 'app-core', 'https://s3-us-west-2.amazonaws.com/instruction.austincc.edu/components/app.js' );
    wp_enqueue_script( 'foundation', 'https://cdnjs.cloudflare.com/ajax/libs/foundation/6.7.5/js/foundation.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/iw-scripts.js', array('jquery'), false, true );
    // wp_enqueue_script( 'scripts', get_theme_file_uri('/js/iw-scripts.js'), array('jquery'), false, true );
  }

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
  }

	function add_to_context( $context ) {
    $context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu('Main Menu');
		$context['site'] = $this;
		return $context;
  }
  
  function add_allowed_tags( $tags ) {
    // $tags['x-hello'] = array(
    //   'class' => true,
    //   'id' => true,
    //   'style' => true
    // );
    
    return $tags;
  }

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		// $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));

    $function = new Twig_SimpleFunction('enqueue_script', function ($handle) {
        // register it elsewhere
        wp_enqueue_script( $handle);
    });
    $twig->addFunction($function);
    $function = new Twig_SimpleFunction('enqueue_style', function ($handle) {
        // register it elsewhere
        wp_enqueue_style( $handle);
    });
    $twig->addFunction($function);

		return $twig;
  }
  
  
  

	/**
	 * Enqueue scripts and styles.
	 */
	// function instructional_web_scripts() {
	// 	wp_enqueue_style( 'wp_enqueue_scripts', get_template_directory_uri() . 'js/app.core.js' );
	// 	wp_enqueue_style( 'wp_enqueue_scripts', get_template_directory_uri() . 'js/app.core.pf.js' );
	// 	wp_enqueue_style( 'wp_enqueue_scripts', get_template_directory_uri() . 'js/foundation-dropdown-menu.js' );
	// 	wp_enqueue_style( 'wp_enqueue_scripts', get_template_directory_uri() . 'js/stencil-route.js' );
	
	// 	wp_enqueue_style( 'wp_enqueue_style',  get_template_directory_uri() . 'css/foundation-dropdown-menu.css' );
	// }
	// add_action( 'wp_enqueue_scripts', 'instructional_web_scripts' );

}

// Allow editors to see access the Menus page under Appearance but hide other options
// Note that users who know the correct path to the hidden options can still access them
function hide_menu() {
  $user = wp_get_current_user();
 
 // Check if the current user is an Editor
 if ( in_array( 'editor', (array) $user->roles ) ) {
   
   // They're an editor, so grant the edit_theme_options capability if they don't have it
   if ( ! current_user_can( 'edit_theme_options' ) ) {
     $role_object = get_role( 'editor' );
     $role_object->add_cap( 'edit_theme_options' );
   }
  
    // Hide the Themes page
     remove_submenu_page( 'themes.php', 'themes.php' );

     // Hide the Widgets page
     remove_submenu_page( 'themes.php', 'widgets.php' );

     // Hide the Customize page
     remove_submenu_page( 'themes.php', 'customize.php' );

     // Remove Customize from the Appearance submenu
     global $submenu;
     unset($submenu['themes.php'][6]);
 }
}

add_action('admin_menu', 'hide_menu', 10);

function iw_customize_register( $wp_customize ) {
  // custom link color
  function checkLink($input) {
    if ($input == '') {
      return '#1a5479';
    }

    return $input;
  }
  $wp_customize->add_setting( 'link_color', array(
    'default' => '#1a5479',
    'sanitize_callback' => 'checkLink'
  ));
  $wp_customize->add_control( 
    new WP_Customize_Control( 
    $wp_customize, 
    'link_color', 
    array(
      'label'      => __( 'Link Color', 'instructional-web' ),
      'section'    => 'colors',
      'settings'   => 'link_color',
    ) ) 
  );

  // custom header background color
  function checkHeader( $input ) {
    if ($input == '') {
      return '#1a5276';
    }

    return $input;
  }
  $wp_customize->add_setting( 'header_color', array(
    'default' => '#1a5276',
    'sanitize_callback' => 'checkHeader'
  ));
  $wp_customize->add_control( 
    new WP_Customize_Control( 
    $wp_customize, 
    'header_color', 
    array(
      'label'      => __( 'Header Background Color', 'instructional-web' ),
      'section'    => 'colors',
      'settings'   => 'header_color',
    ) ) 
  );

  // remove the default color picker
  $wp_customize->remove_control( 'header_textcolor' );

  // custom header text color
  function checkHeaderColor( $input ) {
    if ($input == '') {
      return '#fff';
    }

    return $input;
  }
  $wp_customize->add_setting( 'header_text_color', array(
    'default' => '#fff',
    'sanitize_callback' => 'checkHeaderColor'
  ));
  $wp_customize->add_control( 
    new WP_Customize_Control( 
    $wp_customize, 
    'header_text_color', 
    array(
      'label'      => __( 'Header Text Color', 'instructional-web' ),
      'section'    => 'colors',
      'settings'   => 'header_text_color',
    ) ) 
  );

  // custom menu color
  function checkMenuColor( $input ) {
    if ($input == '') {
      return '#1a5276';
    }

    return $input;
  }
  $wp_customize->add_setting( 'menu_color', array(
    'default' => '#1a5276',
    'sanitize_callback' => 'checkMenuColor'
  ));
  $wp_customize->add_control( 
    new WP_Customize_Control( 
    $wp_customize, 
    'menu_color', 
    array(
      'label'      => __( 'Menu Color', 'instructional-web' ),
      'section'    => 'colors',
      'settings'   => 'menu_color',
    ) ) 
  );


  // custom menu hilite color
  function checkHiliteA( $input ) {
    if ($input == '') {
      return '#6a8da3';
    }

    return $input;
  }
  $wp_customize->add_setting( 'menu_hilite_a', array(
    'default' => '#6a8da3',
    'sanitize_callback' => 'checkHiliteA'
  ));
  $wp_customize->add_control( 
    new WP_Customize_Control( 
    $wp_customize, 
    'menu_hilite_a', 
    array(
      'label'      => __( 'Menu Highlite A', 'instructional-web' ),
      'section'    => 'colors',
      'settings'   => 'menu_hilite_a',
    ) ) 
  );

  // custom menu second level hilite color
  function checkHiliteB( $input ) {
    if ($input == '') {
      return '#1a5276';
    }

    return $input;
  }
  $wp_customize->add_setting( 'menu_hilite_b', array(
    'default' => '#1a5276',
    'sanitize_callback' => 'checkHiliteB'
  ));
  $wp_customize->add_control( 
    new WP_Customize_Control( 
    $wp_customize, 
    'menu_hilite_b', 
    array(
      'label'      => __( 'Menu Highlite B', 'instructional-web' ),
      'section'    => 'colors',
      'settings'   => 'menu_hilite_b',
    ) ) 
  );

  // google analytics
  $wp_customize->add_section('analytics', array(
    'title' => __('Analytics', 'instructional-web'),
    'priority' => 2
  ));
  $wp_customize->add_setting( 'analytics_id', array(
    'default' => ''
  ));

  $wp_customize->add_control( 
    new WP_Customize_Control( 
      $wp_customize, 
      'analytics_id', 
      array(
        'label'      => __( 'Analytics ID', 'instructional-web' ),
        'section'    => 'analytics',
        'settings'   => 'analytics_id',
        'priority'   => 1
      )
    ) 
  );

  // Sticky Nav
  $wp_customize->add_section('sticky_nav', array(
    'title' => __('Sticky Nav', 'instructional-web'),
    'priority' => 10
  ));

  $wp_customize->add_setting( 'sticky_nav_enabled', array(
    'default' => false
  ));

  $wp_customize->add_control( 
    new WP_Customize_Control( 
      $wp_customize, 
      'sticky_nav_visible', 
      array(
        'label'      => __( 'Sticky Nav Enabled', 'instructional-web' ),
        'section'    => 'sticky_nav',
        'settings'   => 'sticky_nav_enabled',
        'type'       => 'checkbox',
        'priority'   => 1
      )
    ) 
  );

  // crm buttons
  $wp_customize->add_section('crm', array(
    'title' => __('CRM', 'instructional-web'),
    'priority' => 10
  ));

  $wp_customize->add_setting( 'crm_visible', array(
    'default' => false
  ));

  $wp_customize->add_control( 
    new WP_Customize_Control( 
      $wp_customize, 
      'crm_visible', 
      array(
        'label'      => __( 'CRM Visibility', 'instructional-web' ),
        'section'    => 'crm',
        'settings'   => 'crm_visible',
        'type'       => 'checkbox',
        'priority'   => 1
      )
    ) 
  );
}


new StarterSite();

// https://wordpress.stackexchange.com/questions/117344/failed-to-import-media
// https://wordpress.stackexchange.com/questions/123307/how-do-i-use-the-http-request-host-is-external-filter/123313#123313

// add_filter( 'http_request_host_is_external', 'allow_my_custom_host', 10, 3 );
// function allow_my_custom_host( $allow, $host, $url ) {
//   if ( $host == 'my-update-server' )
//     $allow = true;
//   return $allow;
// }

add_filter( 'http_request_host_is_external', '__return_true' );


// Override Jetpack OG Twitter description so it doesn't show
//  "Visit the post for more" in link previews

function jetpack_change_og_description( $tags ) {
  // Remove the default blank image added by Jetpack
  unset( $tags['og:description'] );
  $tags['og:description'] = "";
  unset( $tags['twitter:description'] );
  $tags['twitter:description'] = "";

  return $tags;
}
add_filter('jetpack_open_graph_tags', 'jetpack_change_og_description');

/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com/search-wordpress-by-custom-fields-without-a-plugin/
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
  global $wpdb;

  if ( is_search() ) {    
      $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
  }

  return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
* Modify the search query with posts_where
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
*/
function cf_search_where( $where ) {
  global $pagenow, $wpdb;

  if ( is_search() ) {
      $where = preg_replace(
          "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
          "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
  }

  return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
* Prevent duplicates
*
* http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
*/
function cf_search_distinct( $where ) {
  global $wpdb;

  if ( is_search() ) {
      return "DISTINCT";
  }

  return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );
