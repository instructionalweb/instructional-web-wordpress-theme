<?php
/**
 * The Template for displaying all single posts
 * Testing
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$post_cat = $post->get_terms('category');
$post_cat = $post_cat[0]->ID;

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-job-listing' . $post->ID . '.twig', 'single-job-listing' . $post->post_type . '.twig', 'single-job-listing.twig' ), $context );
}
