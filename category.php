<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();
$context['posts'] = new Timber\PostQuery();
$context['footer_widgets'] = Timber::get_widgets( 'footer_widgets' );
$context['header_widgets'] = Timber::get_widgets( 'header_widgets' );
$context['sidebar_widgets'] = Timber::get_widgets( 'sidebar_widgets' );
$context['category_title'] = single_cat_title( '', false );

if ( is_multisite() ) {
	// echo "SiteName " . get_bloginfo( 'name' );
	$blog_title = get_bloginfo( 'name' );
	$context['site_title'] = $blog_title;
}

$templates = array( 'category.twig' );
Timber::render( $templates, $context );
