<?php
/**
 * The Template for displaying all single posts
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */
 $context = array();
 $context['sidebar_widgets'] = Timber::get_widgets('sidebar_widgets');
Timber::render( array( 'sidebar.twig' ), $context );
