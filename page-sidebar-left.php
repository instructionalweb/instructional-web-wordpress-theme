<?php
/**
 * Template Name: Sidebar Left
 * Displays a sidebar on the left side of the main content window
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$context['footer_widgets'] = Timber::get_widgets( 'footer_widgets' );
$context['header_widgets'] = Timber::get_widgets( 'header_widgets' );
$post = new TimberPost();
$context['post'] = $post;
if ( post_password_required( $post->ID ) ) {
    Timber::render( 'single-password.twig', $context );
} else {
    Timber::render( array( 'page-sidebar-left.twig' ), $context );
}