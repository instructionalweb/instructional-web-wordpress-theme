<?php
/**
 * Template Name: Music Events
 * Displays Music Events
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$page_url = Timber\URLHelper::get_current_url();

// Set the start and end dates for the semester based on the page URL
switch ($page_url) {
    case str_contains($page_url, 'spring-24'):
        // $start_date = strtotime('20240101');
        // $end_date = strtotime('20240531');
        $start_date = '20240101';
        $end_date = '20240531';
        break;
    default:
        $start_date = '20200101';
        // Date one year in the future
        $end_date = date('Ymd', strtotime('+1 year'));
        break;
}

function isEventPast($event) {
    $today = date('Ymd');
    if (empty($event->event_date)) {
        return false;
    }
    return $event->event_date < $today;
}

function isEventFuture($event) {
    $today = date('Ymd');
    if (empty($event->event_date)) {
        return false;
    }
    return $event->event_date >= $today;
}

function convertToTimestamp($event) {
    $dateString = $event->event_date;
    $timeString = $event->start_time;

    // Concatenate date and time strings
    $dateTimeString = $dateString . ' ' . $timeString;

    // Convert to Unix timestamp
    $timestamp = strtotime($dateTimeString);

    $event->timestamp = $timestamp;
    return $event;
}

// Used to sort the events by date
function sortByTimestamp($a, $b) {
    if ($a->timestamp == $b->timestamp) {
        return 0;
    }
    return ($a->timestamp <= $b->timestamp) ? -1 : 1;
}


$all_events = Timber::get_posts('post_type=music_events&nopaging=true'); 

// Find all events that are within the current semester
// Some weird PHP syntax for passing params to a function
$all_semester_events = array_filter($all_events, function($event) use ($start_date, $end_date){
    if (empty($event->event_date)) {
        return false;
    }
    // echo $event->event_date . "<br>";
    // echo $start_date . '<br>';
    return $event->event_date >= $start_date && $event->event_date <= $end_date;
});

foreach ($all_events as $event) {
    convertToTimestamp($event);
}

// Split the events into past and future events
$past_events = array_filter($all_semester_events, 'isEventPast');
$future_events = array_filter($all_semester_events, 'isEventFuture');

// Sort the events by timestamp
usort($future_events, "sortByTimestamp");
usort($past_events, "sortByTimestamp");

$context = Timber::get_context();
$context['current_url'] = Timber\URLHelper::get_current_url();
$context['events'] = Timber::get_posts('post_type=music_events&nopaging=true');
$context['past_events'] = $past_events;
$context['future_events'] = $future_events;
$context['footer_widgets'] = Timber::get_widgets( 'footer_widgets' );
$context['header_widgets'] = Timber::get_widgets( 'header_widgets' );
$post = new TimberPost();
$context['post'] = $post;
Timber::render( array( 'page-music-events.twig' ), $context );