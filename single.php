<?php
/**
 * The Template for displaying all single posts
 * Testing
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;
$context['sidebarType'] = get_field('sidebar', 'select');
$post_cat = $post->get_terms('category');
$post_cat = $post_cat[0]->ID;

$sidebar_context = array();
$sidebar_context['related'] = Timber::get_posts('cat='.$post_cat);
$context['sidebar'] = Timber::get_sidebar('sidebar.twig', $sidebar_context);
$context['sidebar_widgets'] = Timber::get_widgets( 'sidebar_widgets' );

if ( is_multisite() ) {
	$blog_title = get_bloginfo( 'name' );
	$context['site_title'] = $blog_title;
}

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
